import Foundation

extension Result {
    
    public var value: Success? {
        return try? get()
    }
    
    public var error: Error? {
        if case .failure(let error) = self {
            return error
        }
        return nil
    }
}
