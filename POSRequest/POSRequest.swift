import Foundation
import Alamofire
import Combine

public struct LogResult {
    public let request: URLRequest
    public let response: HTTPURLResponse
    public let data: Data
    public let json: String
}

final public class POSRequest {
    public enum AuthTokenType {
        case bearer
        case basic
        case digest
        case hoba
        case mutual
        case aws4_hmac_sha256
        case custom(String)
        case none
        
        var value: String {
            switch self {
            case .bearer: return "Bearer"
            case .basic: return "Basic"
            case .digest: return "Digest"
            case .hoba: return "HOBA"
            case .mutual: return "Mutual"
            case .aws4_hmac_sha256: return "AWS4-HMAC-SHA256"
            case .custom(let type): return type
            case .none: return ""
            }
        }
    }
    
    public static var authTokenType: AuthTokenType = .bearer
    
    public static var requestRetrier: RequestRetrier?
    public static var requestAdapter: RequestAdapter?
    
    public static var tokenRefresherConfig: TokenRefresherConfig?
    
    public static var timeoutInterval: TimeInterval = 10
    
    public static var API_PATH: String!
    
    // User token
    public static var sessionToken: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: "user_session_token")
        }
        get {
            return UserDefaults.standard.string(forKey: "user_session_token")
        }
    }
    
    // add header
    
    public static func addHeader(_ hedear: HTTPHeader) {
        removeHeader(for: hedear.name)
        var headers = customHeaders ?? [HTTPHeader]()
        headers.append(hedear)
        customHeaders = headers
    }
    
    // delete key from headers
    
    public static func removeHeader(for key: String) {
        var headers = customHeaders ?? [HTTPHeader]()
        headers.removeAll(where: { $0.name == key })
        customHeaders = headers
    }
    
    // delete all custom headers
    
    public static func deleteAllHeaders() {
        customHeaders = nil
    }
    
    /**
     loggerPublisher to get statusCode and path
     */
    public static var loggerPublisher: AnyPublisher<LogResult, Never> {
        return POSRequest.passthroughSubject.eraseToAnyPublisher()
    }
    
    static var customHeaders: [HTTPHeader]? {
        get {
            guard let data = UserDefaults.standard.data(forKey: "customHeaders"),
                  let headers = try? JSONDecoder().decode([HTTPHeader].self, from: data) else {
                return nil
            }
            return headers
        }
        
        set {
            let defaults = UserDefaults.standard
            if let newValue = newValue,
                let data = try? JSONEncoder().encode(newValue) {
                defaults.set(data, forKey: "customHeaders")
            } else {
                defaults.removeObject(forKey: "customHeaders")
            }
        }
    }
    
    static let passthroughSubject = PassthroughSubject<LogResult, Never>()
    static var doNotRetryCalled = false
}
