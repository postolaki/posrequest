import Foundation
import SwiftyJSON

final public class TokenRefresherConfig {
    let request: URLRequest?
    let unauthorizedCode: Int
    let jsonPathToNewToken: [JSONSubscriptType]?
    let unauthorizedHandler: (() -> Void)?
    let onFailure: (() -> Void)?
    
    public init(refreshTokenRequest: URLRequest,
                unauthorizedCode: Int = 401,
                jsonPath: [JSONSubscriptType],
                onFailure: (() -> Void)? = nil,
                unauthorizedHandler: (() -> Void)? = nil) {
        self.request = refreshTokenRequest
        self.unauthorizedCode = unauthorizedCode
        self.jsonPathToNewToken = jsonPath
        self.onFailure = onFailure
        self.unauthorizedHandler = unauthorizedHandler
    }
    
    public init(unauthorizedCode: Int, unauthorizedHandler: @escaping (() -> Void)) {
        self.request = nil
        self.unauthorizedCode = unauthorizedCode
        self.jsonPathToNewToken = nil
        self.onFailure = nil
        self.unauthorizedHandler = unauthorizedHandler
    }
}
