import Alamofire
import Foundation
import SwiftyJSON

public protocol ResponseSerializable {
    init?(json: JSON)
}

public struct JSONObjectSerializable: ResponseSerializable {
    public let json: JSON
    public init?(json: JSON) {
        self.json = json
    }
}

public typealias JSONSerializable = Result<JSONObjectSerializable, Error>

public extension DataRequest {
    func responseSerializable<Model: ResponseSerializable>(_ completion: @escaping RequestCompletion<Model>) -> DataRequest {
        return makeRequest { response in
            switch response {
            case .success(let data):
                do {
                    let json = try JSON(data: data)
                    if let serializedObject = Model(json: json) {
                        completion(.success(serializedObject))
                    } else {
                        let message = json.rawString(.utf8, options: .prettyPrinted) ?? "unknown"
                        let error = NSError(domain: "POSRequest", code: 0, userInfo: ["error": "Could not parse response: \(message)"])
                        completion(.failure(error as Error))
                    }
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func responseDecodable<Model: Decodable>(_ completion: @escaping RequestCompletion<Model>) -> DataRequest {
        return makeRequest { response in
            switch response {
            case .success(let data):
                do {
                    let model = try JSONDecoder().decode(Model.self, from: data)
                    completion(.success(model))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func makeRequest(completion: @escaping (Result<Data, Error>) -> Void) -> DataRequest {
        POSRequest.doNotRetryCalled = false
        let responseSerializer = DataResponseSerializer()
        return response(responseSerializer: responseSerializer) { object in
            if let response = object.response,
               let request = object.request,
               let data = object.data,
               let json = String(data: data, encoding: .utf8) {
                let logResult = LogResult(request: request, response: response, data: data, json: json)
                POSRequest.passthroughSubject.send(logResult)
            }
            switch object.result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                guard error.responseCode != NSURLErrorCancelled else { return }
                if let data = object.data,
                   let json = try? JSON(data: data) {
                    let nsError = (error as NSError)
                    let newError = NSError(domain: nsError.domain, code: nsError.code, userInfo: json.dictionaryObject)
                    completion(.failure(newError as Error))
                } else {
                    completion(.failure(error))
                }
            }
        }.validate()
    }
}
