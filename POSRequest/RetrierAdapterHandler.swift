import Foundation
import Alamofire
import SwiftyJSON
import Combine

final class RetrierAdapterHandler: RequestRetrier {
    
    private let tokenRefresherConfig: TokenRefresherConfig
    private let lock = NSLock()
    private var isRefreshing = false
    private let maxTries = 2
    
    init(config: TokenRefresherConfig) {
        tokenRefresherConfig = config
    }
    
    func retry(_ request: Alamofire.Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard !POSRequest.doNotRetryCalled else {
            return completion(.doNotRetry)
        }
        lock.lock(); defer { lock.unlock() }
        
        if error.asAFError?.responseCode == tokenRefresherConfig.unauthorizedCode {
            tokenRefresherConfig.unauthorizedHandler?()
            return completion(.doNotRetryWithError(error))
        }
        
        guard request.retryCount < maxTries else {
            tokenRefresherConfig.onFailure?()
            return completion(.doNotRetry)
        }
        refreshToken(session: session, completion: completion)
    }
    
    private func refreshToken(session: Session, completion: @escaping (RetryResult) -> Void) {
        guard !isRefreshing else {
            POSRequest.doNotRetryCalled = true
            return completion(.doNotRetry)
        }
        guard let request = tokenRefresherConfig.request,
              let jsonPathToNewToken = tokenRefresherConfig.jsonPathToNewToken else {
            tokenRefresherConfig.onFailure?()
            POSRequest.doNotRetryCalled = true
            return completion(.doNotRetry)
        }
        isRefreshing = true
        guard POSRequest.sessionToken != nil else {
            tokenRefresherConfig.onFailure?()
            POSRequest.doNotRetryCalled = true
            return completion(.doNotRetry)
        }
        
        session.request(request).responseData { [weak self] dataResponse in
            self?.lock.lock() ; defer { self?.lock.unlock() }
            
            guard let data = dataResponse.value,
                  let json = try? JSON(data: data) else {
                self?.tokenRefresherConfig.onFailure?()
                POSRequest.doNotRetryCalled = true
                return completion(.doNotRetry)
            }
            
            self?.isRefreshing = false
            if let newToken = json[jsonPathToNewToken].string {
                POSRequest.sessionToken = newToken
                completion(.retry)
            } else {
                self?.tokenRefresherConfig.onFailure?()
                POSRequest.doNotRetryCalled = true
                completion(.doNotRetry)
            }
        }
    }
}
