import Foundation
import Alamofire

final class RequestAdapterHandler: RequestAdapter {
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var urlRequest = urlRequest
        // "CanBeUpdateByAdapter" header is set when creatign the request
        if let adapterUpdate = urlRequest.allHTTPHeaderFields?["CanBeUpdatedByAdapter"],
            let canUpdate = Bool(adapterUpdate), canUpdate == true,
            let token = POSRequest.sessionToken, !token.isEmpty {
            
            let tokenType = POSRequest.authTokenType
            switch tokenType {
            case .none:
                urlRequest.setUserAuthorizationHeader(token: token)
            default:
                urlRequest.setUserAuthorizationHeader(token: "\(tokenType.value) \(token)")
            }
        }

        completion(.success(urlRequest))
    }
}
