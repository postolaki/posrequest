import Foundation
import SwiftyJSON
import Alamofire

// MARK: - Params

public typealias HTTPMethod = Alamofire.HTTPMethod
public typealias HTTPHeader = Alamofire.HTTPHeader

public extension PSRequest {
    enum Params {
        
        case query([String: Any])
        case body([String: Any])
        case bodyData(Any)
        
        public var value: Any {
            switch self {
            case .body(let params): return params
            case .query(let params): return params
            case .bodyData(let params): return params
            }
        }
    }
}

// MARK: - Request

public struct PSRequest: URLRequestConvertible {
    
    public let method: HTTPMethod
    public let url: APIURL
    public var params: [Params]?
    public let authenticationToken: AuthenticationToken
    public let headers: [HTTPHeader]?
        
    public init(_ method: HTTPMethod = .get,
                url: APIURL, params: [Params]? = nil,
                authenticationToken: AuthenticationToken = .userSession,
                headers: [HTTPHeader]? = nil) {
        self.method = method
        self.url = url
        self.params = params
        self.authenticationToken = authenticationToken
        self.headers = headers
    }
    
    public func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: self.url.absolute)
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = POSRequest.timeoutInterval
        
        // set request parameters
        for param in params ?? [] {
            switch param {
            case .query(let value):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: value)
            case .body(let value):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: value)
            case .bodyData(let value):
                urlRequest = try JSONEncoding.default.encode(urlRequest, withJSONObject: value)
            }
        }
        
        setAuthentication(token: authenticationToken, urlRequest: &urlRequest)
        if let customHeaders = POSRequest.customHeaders {
            customHeaders.forEach {
                urlRequest.setValue($0.value, forHTTPHeaderField: $0.name)
            }
        }
        
        if let headers {
            headers.forEach {
                urlRequest.setValue($0.value, forHTTPHeaderField: $0.name)
            }
        }
        
        return urlRequest
    }
    
    private func setAuthentication(token: AuthenticationToken, urlRequest: inout URLRequest) {
        
        switch token {
        case .userSession:
            if let token = POSRequest.sessionToken {
                urlRequest.setUserAuthorizationHeader(token: "Bearer \(token)")
            }
            urlRequest.allowUpdateTokenByAdapter(allow: true)
            
        case .custom(let token):
            urlRequest.setUserAuthorizationHeader(token: token)
            urlRequest.allowUpdateTokenByAdapter(allow: false)
        case .none:
            urlRequest.allowUpdateTokenByAdapter(allow: false)
        }
    }
}

// MARK: - extension URLRequest

extension URLRequest {
    public mutating func setUserAuthorizationHeader(token: String) {
        setValue(token, forHTTPHeaderField: "Authorization")
    }
    
    /// Allow adapter to update the Authorization token if session expires
    public mutating func allowUpdateTokenByAdapter(allow: Bool) {
        setValue(String(allow), forHTTPHeaderField: "CanBeUpdatedByAdapter")
    }
}

// MARK: - AuthenticationToken

public enum AuthenticationToken {
    case userSession
    case none
    case custom(String)
}

extension HTTPHeader: Codable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(value, forKey: .value)
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let name = try container.decode(String.self, forKey: .name)
        let value = try container.decode(String.self, forKey: .value)
        self = HTTPHeader(name: name, value: value)
    }
    
    enum CodingKeys: String, CodingKey {
        case name, value
    }
}
