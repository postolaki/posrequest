import Alamofire
import Foundation

final class ClientSessionManager {
    
    static let shared = ClientSessionManager()
    
    lazy var clientSessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        configuration.urlCache = nil
        configuration.httpCookieStorage = HTTPCookieStorage.shared

        let adapter: RequestAdapter
        if let requestAdapter = POSRequest.requestAdapter {
            // set adapter from extern if exists
            adapter = requestAdapter
        } else {
            // set default adapter
            adapter = RequestAdapterHandler()
        }
        
        let retrier: RequestRetrier
        if let requestRetrier = POSRequest.requestRetrier {
            // set retrier from extern if exists
            retrier = requestRetrier
        } else if let tokenRefresherConfig = POSRequest.tokenRefresherConfig {
            // set default adapter if config exist
            retrier = RetrierAdapterHandler(config: tokenRefresherConfig)
        } else {
            retrier = EmptyRetrier()
        }
        let interceptor = Interceptor(adapter: adapter, retrier: retrier)
        let manager = Session(configuration: configuration, interceptor: interceptor)
        
        return manager
    }()
}

private class EmptyRetrier: RequestRetrier {
    func retry(_ request: Alamofire.Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        completion(.doNotRetry)
    }
}
