import Foundation
import Alamofire
import SwiftyJSON

/// All routers must be in API enum extension
public enum API { }

// MARK: - APIURL

open class APIURL {
    public let absolute: URL
    
    public init(path: String) {
        let url = URL(string: POSRequest.API_PATH)!
        absolute = url.appendingPathComponent(path)
    }
    
    public init(url: URL) {
        absolute = url
    }
}

// MARK: - RequestInterface

public protocol RequestInterface {
    var method: HTTPMethod { get }
    var url: APIURL { get }
    var params: [PSRequest.Params]? { get }
    var authenticationToken: AuthenticationToken { get }
    var headers: [HTTPHeader]? { get }
}

public typealias RequestCompletion<T> = (Result<T, Error>) -> Void

public extension RequestInterface {
    /**
     It makes a Decodable request.
     - parameter callback: (Result<T, Error>) -> Void where T is Decodable
     */
    @discardableResult
    func request<T: Decodable>(completition: @escaping RequestCompletion<T>) -> DataRequest {
        let manager = ClientSessionManager.shared.clientSessionManager
        let request = PSRequest(method, url: url, params: params, authenticationToken: authenticationToken, headers: headers)
        return manager.request(request).responseDecodable(completition)
    }
    
    /**
     It makes a ResponseSerializable request.
     - parameter callback: (Result<T, Error>) -> Void where T is ResponseSerializable
     */
    @discardableResult
    func request<T: ResponseSerializable>(completition: @escaping RequestCompletion<T>) -> DataRequest {
        let manager = ClientSessionManager.shared.clientSessionManager
        let request = PSRequest(method, url: url, params: params, authenticationToken: authenticationToken, headers: headers)
        return manager.request(request).responseSerializable(completition)
    }
}

public extension PSRequest {
    /**
     It makes a Decodable request.
     - parameter callback: (Result<T, Error>) -> Void where T is Decodable
     */
    @discardableResult
    func makeRequest<T: Decodable>(completition: @escaping RequestCompletion<T>) -> DataRequest {
        let manager = ClientSessionManager.shared.clientSessionManager
        return manager.request(self).responseDecodable(completition)
    }
    
    /**
     It makes a ResponseSerializable request.
     - parameter callback: (Result<T, Error>) -> Void where T is ResponseSerializable
     */
    @discardableResult
    func makeRequest<T: ResponseSerializable>(completition: @escaping RequestCompletion<T>) -> DataRequest {
        let manager = ClientSessionManager.shared.clientSessionManager
        return manager.request(self).responseSerializable(completition)
    }
}
