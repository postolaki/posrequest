//
//  POSRequest.h
//  POSRequest
//
//  Created by Ivan
//  Copyright © 2019 Postolaki. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for POSRequest.
FOUNDATION_EXPORT double POSRequestVersionNumber;

//! Project version string for POSRequest.
FOUNDATION_EXPORT const unsigned char POSRequestVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <POSRequest/PublicHeader.h>
