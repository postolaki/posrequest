import Foundation
import POSRequest
import SwiftyJSON

struct CountryModel: ResponseSerializable {
    let name: String
    let alpha2Code: String
    let alpha3Code: String
    let callingCodes: [String]
    let capital: String
    let region: String
    let subregion: String
    let population: Float
    let demonym: String
    let area: Float?
    let timezones: [String]
    let borders: [String]
    let nativeName: String
    let numericCode: String?
    let currencies: [Currency]
    let languages: [Language]
    let flag: URL
    let cioc: String?
    
    init?(json: JSON) {
        name = json["name"].stringValue
        alpha2Code = json["alpha2Code"].stringValue
        alpha3Code = json["alpha3Code"].stringValue
        callingCodes = json["callingCodes"].arrayValue.map { $0.stringValue }
        capital = json["capital"].stringValue
        region = json["region"].stringValue
        subregion = json["subregion"].stringValue
        population = json["population"].floatValue
        demonym = json["demonym"].stringValue
        area = json["area"].float
        timezones = json["timezones"].arrayValue.map { $0.stringValue }
        borders = json["borders"].arrayValue.map { $0.stringValue }
        nativeName = json["nativeName"].stringValue
        numericCode = json["numericCode"].string
        currencies = json["currencies"].arrayValue.compactMap { Currency(json: $0) }
        languages = json["languages"].arrayValue.compactMap { Language(json: $0) }
        flag = json["flag"].url!
        cioc = json["cioc"].string
    }
}

struct Currency: ResponseSerializable {
    let code: String?
    let name: String?
    let symbol: String?

    init?(json: JSON) {
        code = json["code"].string
        name = json["name"].string
        symbol = json["symbol"].string
    }
}

struct Language: ResponseSerializable {
    let iso639_1: String?
    let iso639_2: String
    let name: String
    let nativeName: String
    
    init?(json: JSON) {
        iso639_1 = json["iso639_1"].string
        iso639_2 = json["iso639_2"].stringValue
        name = json["name"].stringValue
        nativeName = json["nativeName"].stringValue
    }
}
