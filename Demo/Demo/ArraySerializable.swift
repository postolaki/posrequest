import POSRequest
import SwiftyJSON

typealias CountriesSerializable = Result<ArraySerializable<CountryModel>, Error>

class ArraySerializable<T: ResponseSerializable>: ResponseSerializable {
    var elements: [T]
    
    required public init?(json: JSON) {
        elements = json.arrayValue.compactMap({ T(json: $0) })
    }
}
