import UIKit
import POSRequest
import Combine

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var bag = Set<AnyCancellable>()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        POSRequest.API_PATH = "https://restcountries.com/v3.1"
        POSRequest.addHeader(HTTPHeader(name: "Accept", value: "application/json"))
        
        handleUnauthorized()
        
        POSRequest.loggerPublisher
            .sink { logResult in
                let url = logResult.request.url!.absoluteURL
                let statusCode = logResult.response.statusCode
                print("Request to url \(url) ended with status \(statusCode).")
            }.store(in: &bag)
        
        return true
    }
    
    private func handleUnauthorized() {
//        let url = URL(string: "refresh token url")!
//        let request = URLRequest(url: url)
//        let config = TokenRefresherConfig(refreshTokenRequest: request,
//                                          unauthorizedCode: 401,
//                                          jsonPath: ["new", "token", "path"]) {
//            // logout if need
//        }
        let config = TokenRefresherConfig(unauthorizedCode: 401) {
            print("should logout")
        }
        POSRequest.tokenRefresherConfig = config
    }
}
