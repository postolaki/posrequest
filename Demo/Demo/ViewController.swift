import UIKit
import POSRequest

typealias CountriesDecodable = Result<[CountryModelDecodable], Error>

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // test the request with SwiftyJSON
        API.CountryRequest.getAll.request { (response: CountriesSerializable) in
            switch response {
            case .success(let value):
                print(value.elements)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }

        // test the request with Decodable
//        API.CountryRequest.getAll.request { (response: CountriesDecodable) in
//            switch response {
//            case .success(let value):
//                print(value)
//            case .failure(let error):
//                print(error)
//            }
//        }
        
        // make request without inherit from API enum
//        let request = PSRequest(.get, url: APIURL("all"), authenticationToken: .none)
//        request.makeRequest { (response: CountriesDecodable) in
//            switch response {
//            case .success(let value):
//                print(value)
//            case .failure(let error):
//                print(error)
//            }
//        }
//
//        request.makeRequest { (response: CountriesSerializable) in
//            switch response {
//            case .success(let value):
//                print(value.elements)
//            case .failure(let error):
//                print(error.localizedDescription as Any)
//            }
//        }
    }
}
