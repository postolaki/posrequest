import Foundation
import POSRequest

extension API {
    enum CountryRequest: RequestInterface {
        
        case getAll
        
        var method: HTTPMethod {
            return .get
        }
        
        var url: APIURL {
            return APIURL(path: "all")
        }
        
        var params: [PSRequest.Params]? {
            return nil
        }
        
        var authenticationToken: AuthenticationToken {
            return .none
        }
        
        var headers: [HTTPHeader]? {
            return nil
        }
    }
}
