// swift-tools-version:5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(name: "POSRequest",
                      platforms: [.iOS(.v13)],
                      products: [.library(name: "POSRequest",
                                          targets: ["POSRequest"])],
                      dependencies: [
                        .package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", .upToNextMajor(from: "5.0.1")),
                        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.6.2"))
                      ],
                      targets: [.target(name: "POSRequest",
                                        dependencies: ["SwiftyJSON", "Alamofire"],
                                        path: "POSRequest",
                                        exclude: ["Info.plist", "POSRequest.h"])
                      ],
                      swiftLanguageVersions: [.v5])
