Pod::Spec.new do |s|
  s.platform = :ios
  s.ios.deployment_target = '13.0'
  s.name = "POSRequest"
  s.summary = "POSRequest"
  s.requires_arc = true

  s.version = "1.0.12"
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.author  = { "Ivan" => "ivan.postolaki@gmail.com" }
  s.homepage = "https://bitbucket.org/postolaki/posrequest"
  s.source = { :git => "https://postolaki@bitbucket.org/postolaki/posrequest.git", :tag => s.version.to_s }

  s.swift_version = "5"
  s.frameworks = "Foundation", "UIKit"

  s.source_files = "POSRequest/**/*.{swift}"
  s.dependency "Alamofire"
  s.dependency "SwiftyJSON"
end
