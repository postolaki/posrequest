# POSRequest

## Example

To run the example project, clone the repo, and run `pod install` from the Demo directory first.

## Requirements
- [x] Xcode 10
- [x] Swift 5
- [x] iOS 9 or higher

## Installation

POSRequest is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'POSRequest'
```

```swift
import POSRequest

POSRequest.API_PATH = "..."

// test the request with SwiftyJSON
API.CountryRequest.getAll.request { (response: CountrySerializable) in
    switch response {
    case .success(let value):
        print(value.elements)
    case .failure(let error):
        print(error.localizedDescription as Any)
    }
}

// test the request with Decodable
API.CountryRequest.getAll.request { (response: ResponseResult<[CountryModelDecodable]>) in
    switch response {
    case .success(let value):
        print(value)
    case .failure(let error):
        print(error)
    }
}

// refresh token
let url = URL(string: "refresh token url")!
let request = URLRequest(url: url)
let config = TokenRefresherConfig(refreshTokenRequest: request,
                                  unauthorizedCode: 401,
                                  jsonPath: ["new", "token", "path"]) {
                                    // logout if need
}
POSRequest.tokenRefresherConfig = config
```

## Config

```swift
POSRequest
    public enum AuthTokenType {
        case bearer
        case basic
        case digest
        case hoba
        case mutual
        case aws4_hmac_sha256
        case custom(String)
        case none
        
        var value: String {
            switch self {
            case .bearer: return "Bearer"
            case .basic: return "Basic"
            case .digest: return "Digest"
            case .hoba: return "HOBA"
            case .mutual: return "Mutual"
            case .aws4_hmac_sha256: return "AWS4-HMAC-SHA256"
            case .custom(let type): return type
            case .none: return ""
            }
        }
    }

    public static var authTokenType: AuthTokenType = .bearer

    public static var requestRetrier: RequestRetrier?
    public static var requestAdapter: RequestAdapter?

    public static var tokenRefresherConfig: TokenRefresherConfig?

    public static var timeoutInterval: TimeInterval = 10

    public static var API_PATH: String!

    public static var sessionToken: String?

    public static func addHeader(key: String, value: String)

    public static func removeHeader(key: String)

    public static func deleteAllHeaders()

ErrorResponse
    public static func appendErrorPath(_ jsonPath: [JSONSubscriptType])
    public static func appendErrorPaths(_ jsonPaths: [[JSONSubscriptType]])
```

## Author

ivan.postolaki@gmail.com

## License

POSRequest is available under the MIT license.